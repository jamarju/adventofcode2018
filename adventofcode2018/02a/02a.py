#!/usr/bin/env python2.7

import sys


def main(argv):
    if len(argv) <= 1:
        sys.stderr.write(argv[0] + ": missing testfile\n")
        exit(1)
    else:
        testfile = argv[1]

    n2 = n3 = 0
    with open(testfile) as fin:
        for line in fin:
            d = {}
            for c in line.rstrip():
                if c not in d:
                    d[c] = 1
                else:
                    d[c] += 1
            for c, n in d.items():
                if n == 2:
                    n2 += 1
                    break
            for c, n in d.items():
                if n == 3:
                    n3 += 1
                    break
    print(n2 * n3)


if __name__ == "__main__":
    main(sys.argv)
