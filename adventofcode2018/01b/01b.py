#!/usr/bin/env python2.7

import sys


def main(argv):
    if len(argv) <= 1:
        sys.stderr.write(argv[0] + ": missing testfile\n")
        exit(1)
    else:
        testfile = argv[1]

    s = 0
    fs = set()
    while True:
        with open(testfile) as fin:
            for line in fin:
                s += int(line)
                if s in fs:
                    print(s)
                    return
                fs.add(s)


if __name__ == "__main__":
    main(sys.argv)
