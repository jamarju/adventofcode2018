#!/usr/bin/env python2.7

import sys

def cmp(s1, s2):
    d = 0
    for i in range(len(s1)):
        if s1[i] != s2[i]:
            d += 1
    return d


with open("input.txt") as f:
    lines = [ l.rstrip() for l in f.readlines() ]
    for i in range(len(lines)):
        for j in range(i + 1, len(lines)):
            if cmp(lines[i], lines[j]) == 1:
                for k in range(len(lines[i])):
                    if lines[i][k] == lines[j][k]:
                        print(lines[i][k], end='')
