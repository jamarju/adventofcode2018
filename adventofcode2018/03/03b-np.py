import re
import numpy as np

p = re.compile('#(\d+) @ (\d+),(\d+): (\d+)x(\d+)')
m = np.zeros((1000, 1000), dtype=int)

for line in open("input.txt"):
    cid, x, y, w, h = map(int, p.search(line).groups())
    m[y:y+h, x:x+w] += 1
print((m > 1).sum())

for line in open("input.txt"):
    cid, x, y, w, h = map(int, p.search(line).groups())
    if np.all(m[y:y+h, x:x+w] == 1):
        print(cid)
        break
